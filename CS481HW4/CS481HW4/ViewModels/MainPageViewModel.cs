﻿using CS481HW4.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Diagnostics;

namespace CS481HW4.ViewModels
{

    public class MainPageViewModel : BindableBase, INavigatingAware
    {
        //Navigation Service
        INavigationService _navigationService;

        //Button Delegates
        public DelegateCommand LearningButton { get; set; }
        public DelegateCommand CardButton { get; set; }

        //Title
        public string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainPageViewModel(INavigationService navigationService)
        {   
            //CONSTRUCTOR
            Debug.WriteLine($"**** {this.GetType().Name}: ctor");

            Title = "Launch Page";
            _navigationService = navigationService;
            LearningButton = new DelegateCommand(OnLearnPressed);
            CardButton = new DelegateCommand(OnCardPressed);
        }

        private void OnLearnPressed()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnLearnPressed)}");

            _navigationService.NavigateAsync(nameof(LearningPage));
        }

        private void OnCardPressed()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCardPressed)}");

            _navigationService.NavigateAsync(nameof(CardPage));

        }

        #region INavigationAware implementation

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }

        #endregion INavigationAware implementation
    }


}