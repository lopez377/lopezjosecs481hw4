﻿using CS481HW4.ViewModels;
using CS481HW4.Views;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;


namespace CS481HW4
{
	public partial class App : PrismApplication
	{

        public  App(IPlatformInitializer initializer = null) : base(initializer) {  }

		protected override void OnStart ()
		{
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
        }

		protected override void OnSleep ()
		{
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            // Handle when your app sleeps
        }

		protected override void OnResume ()
		{
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            // Handle when your app resumes
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");

            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<LearningPage, MainPageViewModel>(); //Registered my Learning Page for Navigation
            containerRegistry.RegisterForNavigation<CardPage, MainPageViewModel>(); //Registered my Card Page for Navigation
        }

        protected override void OnInitialized()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            NavigationService.NavigateAsync(nameof(MainPage));
        }
    }
}
